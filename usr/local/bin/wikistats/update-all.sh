#! /bin/bash

# Full 29 Mediawiki families:
# an et ga lx mh mt mw ne os re ro sc sf si sw un w3 wb wf wi wn wp wq wr ws wt wv wx wy

# Excluded:
# wi: Wikias, too large
# si: Wikisite, isn't working

# Larges (>500) moved last: et mh mw

for code in an ga lx mt ne os re ro sc sf sw un w3 wb wf wn wq wr ws wt wv wx wy et mh mw; do
        /usr/lib/wikistats/update.php $code
done
